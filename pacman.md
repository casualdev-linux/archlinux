## [Pacman](https://wiki.archlinux.org/title/Pacman)
### Enable parallel downloads
Pacman 6.0 introduced the option to download packages in parallel. ParallelDownloads under [options] needs to be set to a positive integer in /etc/pacman.conf to use this feature (e.g., 5). Packages will otherwise be downloaded sequentially if this option is unset.


`sudo pacman -S git reflector`
`etc/xdg/reflector/reflector.conf`

#### AUR helper
```
git clone https://aur.archlinux.org/yay.git
cd yay/
makepkg -si
```