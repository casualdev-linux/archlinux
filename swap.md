## [Swap](https://wiki.archlinux.org/title/Swap)
[Memory Paging](https://en.wikipedia.org/wiki/Memory_paging)

Linux divides its physical RAM (random access memory) into chunks of memory called pages. Swapping is the process whereby a page of memory is copied to the preconfigured space on the hard disk, called swap space, to free up that page of memory. The combined sizes of the physical memory and the swap space is the amount of virtual memory available.

There are 3 ways to create swaps:
- partition
- file
- zram device

It is recommended to create swaps if the installed Physical memory (RAM) less than memory required to run all the desired programs. 
To check swap status, use:

```
$ swapon --show
```

Or to show physical memory as well as swap usage:

```
$ free -h
```
### Swap partition

To set up a partition as Linux swap area, the [mkswap(8)](https://man.archlinux.org/man/mkswap.8) command is used. For example:

```
# mkswap /dev/sdxy
```
*Warning: All data on the specified partition will be lost.*

To enable the device for paging:

```
# swapon /dev/sdxy
```

To enable this swap partition on boot, add an entry to /etc/fstab:

```
UUID=device_UUID none swap defaults 0 0
```

### Swap file
As an alternative to creating an entire partition, a swap file offers the ability to vary its size on-the-fly, and is more easily removed altogether. This may be especially desirable if disk space is at a premium (e.g. a modestly-sized SSD).

Use mkswap(8) to create a swap file the size of your choosing (see Partitioning#Swap for advice). For example, creating a 4 GiB swap file:

```# mkswap -U clear --size 4G --file /swapfile```

Activate the swap file:

```# swapon /swapfile```

Finally, edit the fstab configuration to add an entry for the swap file:

```
/swapfile none swap defaults 0 0
```

### Zram device
zram-generator provides a systemd-zram-setup@.service unit to automatically initialize zram devices without users needing to enable/start the template or its instances.

To create a zram swap device using zstd and half of the entire available RAM, install zram-generator, then create `/etc/systemd/zram-generator.conf` with the following:

`/etc/systemd/zram-generator.conf`
```
[zram0]
zram-size = ram / 4
compression-algorithm = zstd

[zram1]
zram-size = ram / 4
compression-algorithm = zstd

[zram2]
zram-size = ram / 4
compression-algorithm = zstd

[zram3]
zram-size = ram / 4
compression-algorithm = zstd
```
