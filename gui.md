## plymouth
`plymouth`

`/etc/mkinitcpio.conf`
`HOOKS=(... plymouth ...)`

## graphics
### Display server
`xorg wayland`
### Display drivers
mesa
lib32-mesa
xf86-video-amdgpu
vulkan-radeon
libva-mesa-driver
lib32-libva-mesa-driver
mesa-vdpau
lib32-mesa-vdpau

`/etc/mkinitcpio.conf`
`MODULES=(... amdgpu ...)`

## audio
pipewire
pipewire-jack
wireplumber
qpwgraph


## de/wm
`sddm wayland xorg-xwayland kwin plasma-wayland-session`