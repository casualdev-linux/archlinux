# Arch Linux Installation Guide

This guide exists so that I can compile the installation process, packages and things I do to set up Arch linux for laptop, desktop and server. The objective to write this document is to once and for all organize the necessary information at one place regarding these setups. This should include whats, whys and hows wherever needed.


## Pre-installation
Before installing, make sure to:
- Read the [official wiki](https://wiki.archlinux.org/). It is advisable to read that instead. I wrote this guide for myself.
- Acquire an installation image from [here](https://archlinux.org/download/).
- Verify signature.
  - On a system with GnuPG installed, do this by downloading the ISO PGP signature (under Checksums in the page Download) to the ISO directory, and verifying it with:
  ```bash
  $ gpg --keyserver-options auto-key-retrieve --verify archlinux-version-x86_64.iso.sig
  ```
  - Alternatively, from an existing Arch Linux installation run:
  ```bash
  $ pacman-key -v archlinux-version-x86_64.iso.sig
  ```

- Prepare an installation medium.
## Boot the live environment.
**Note**: Arch Linux installation images do not support Secure Boot. You will need to disable Secure Boot to boot the installationmedium. If desired, Secure Boot can be set up after completing the installation.[read more](https://wiki.archlinux.org/title/Installation_guide)

### Set the console keyboard layout, font and other stuff.
The default [console keymap](https://wiki.archlinux.org/title/Console_keymap "Console keymap") is [US](https://en.wikipedia.org/wiki/File:KB_United_States-NoAltGr.svg "wikipedia:File:KB United States-NoAltGr.svg"). I'm going to keep it US.
```bash
root@archiso~# localectl list-keymaps  // to check available keymaps
root@archiso~# loadkeys us             // load keymap
```

I'll change the default [Console font](https://wiki.archlinux.org/title/Console_fonts "Console font")
```bash
root@archiso~# showconsolefont                      // to see the font console is using
root@archiso~# ls /usr/share/kbd/consolefonts/      // to see the available fonts
root@archiso~# setfont ter-120n                     // to set the font
```

### Verify the Boot mode, check the UEFI bitness:
```bash
root@archiso~# cat /sys/firmware/efi/fw_platform_size
```

If the command returns `64`, then system is booted in UEFI mode and has a 64-bit x64 UEFI. If the command returns `32`, then system is booted in UEFI mode and has a 32-bit IA32 UEFI; while this is supported, it will limit the boot loader choice to systemd-boot. If the file does not exist, the system may be booted in [BIOS](https://en.wikipedia.org/wiki/BIOS "wikipedia:BIOS") (or [CSM](https://en.wikipedia.org/wiki/Compatibility_Support_Module "wikipedia:Compatibility Support Module")) mode. If the system did not boot in the mode you desired (UEFI vs BIOS), refer to your motherboard's manual.


### Connect to the internet
I'm configuring wifi. For lan, use `ip link`.
```bash
root@archiso~# iwctl                     //to start iwctl utility
-[iwd]# device list                       //list wifi devices, mine is wlan0
-[iwd]# station wlan0 get-networks        //get the available network list
-[iwd]# station wlan0 connect *wifi_name* //connect to wifi and enter password on prompt
-[iwd]# exit

root@archiso~# ping archlinux.org        // test the connection
```
### Update system clock
Use [timedatectl(1)](https://man.archlinux.org/man/timedatectl.1) to ensure the system clock is accurate:

- (Optional) Edit the `/etc/pacman.conf` by removing `#` from `#PARALLELDOWNLOADS = 5`. This is only to enable parallel downloads.
  
- (Optional) Make a backup of `/etc/pacman.d/mirrorlist`. Then update the mirrorlist using `reflector`

  ```bash
  root@archiso~# timedatectl
  root@archiso~# cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
  root@archiso~# reflector --latest 20 --protocol https --sort rate --country India,Singapore, --save /etc/pacman
  ```

### Partition the disk
#### Scheme
For my parition scheme I'll create UEFI/GPT layout based scheme.

- Boot: /dev/sda1 - /boot - EFI boot partition - 1G - ef00
- Swap: ~= Ram size, optional
- Root: /dev/sda2 - / - Linux x86-64 /root - 100G - 8304
- Home: /dev/sda3 - /home - Linux /home - rest of the disk - 8302

  ```bash
  root@archiso~# fdisk -l
  root@archiso~# gdisk /dev/sda
  ```

  `gdisk` is a simple utility. Use `d` to delete a partition, `n` to create a new one. `w` to write the changes. I'll use the following hex codes

  - EFI boot partition - 1G - ef00
  - Linux x86-64 /root - 100G - 8304
  - Linux /home - 364.8G - 8302

#### Format the partition and mount

#### Format partition and mount
```bash
root@archiso~# mkfs.fat -F 32 /dev/sda1
root@archiso~# mkfs.btrfs -L root /dev/sda2
root@archiso~# mkfs.btrfs -L home /dev/sda3
root@archiso~# mount /dev/sda2 /mnt
root@archiso~# mount --mkdir /dev/sda1 /mnt/boot
root@archiso~# mount --mkdir /dev/sda3 /mnt/home
```

## Installation
```
root@archiso~# pacstrap -K /mnt base base-devel linux linux-zen linux-firmware amd-ucode networkmanager
```

### Generate fstab
```bash
root@archiso~# genfstab -U /mnt >> /mnt/etc/fstab
```
Check the resulting /mnt/etc/fstab file, and edit it in case of errors.

### Chroot
Now, change root into the newly installed system
```bash
root@archiso~# arch-chroot /mnt /bin/bash
```

### Time Zone
A selection of timezones can be found under /usr/share/zoneinfo/. Since I am in the India, I will be using /usr/share/zoneinfo/Asia/Kolkata. Select the appropriate timezone for your country

`[root@archiso /]# ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime`

Run hwclock to generate /etc/adjtime:

`[root@archiso /]# hwclock --systohc`

This command assumes the hardware clock is set to UTC.

#### Localisation

The locale defines which language the system uses, and other regional considerations such as currency denomination, numerology, and character sets. Possible values are listed in `/etc/locale.gen`. Uncomment en_US.UTF-8, as well as other needed localisations.

Uncomment `en_US.UTF-8 UTF-8` and other needed locales in `/etc/locale.gen`, save, and generate them with:

`[root@archiso /]# locale-gen`

Create the locale.conf file, and set the LANG variable accordingly:

`[root@archiso /]# echo LANG=en_US.UTF-8 > /etc/locale.conf`

#### Network configuration

Create the hostname file. In this guide I'll just use MYHOSTNAME as hostname. Hostname is the host name of the host.
`[root@archiso /]# echo "MYHOSTNAME" > /etc/hostname`

Open /etc/hosts to add matching entries to hosts:
```bash
127.0.0.1    localhost
::1          localhost
127.0.1.1    MYHOSTNAME.localdomain	  MYHOSTNAME
```

If the system has a permanent IP address, it should be used instead of 127.0.1.1.

##### Install the boot loader
I will install systemd and in /boot mountpoint (/dev/sda1 partition).

`bootctl --path=/boot install`

Create the following boot entries 
1. `/boot/loader/entries/arch.conf`
```bash
title Arch Linux  
linux /vmlinuz-linux-zen
initrd /amd-ucode.img
initrd  /initramfs-linux-zen.img  
options root=/dev/sda2 rw
```
2. `/boot/loader/entries/arch-fallback.conf`
```bash
title Arch Linux Fallback
linux /vmlinuz-linux
initrd /amd-ucode.img
initrd  /initramfs-linux.img  
options root=/dev/sda2 rw
```
If your `/` is not in `/dev/sda2`, make sure to change it.Save and exit.
Also, make sure to hook the microcode into kernel in `/etc/mkinitcpio.conf`

`HOOKS=(... amd-ucode ...)`


### Update boot loader configuration
Update bootloader configuration
`vim /boot/loader/loader.conf`
```bash
default arch.conf
timeout 0
console-mode keep
editor no
```

### Enable internet connection for the next boot

To enable the network daemons on your next reboot, you need to enable dhcpcd.service for wired connection and iwd.service for a wireless one.
`systemctl enable NetworkManager`

#### Root password

Set the root password:
`[root@archiso /]# passwd`

#### Add a user account
Add a new user account. In this guide, I'll just use MYUSERNAME as the username of the new user aside from root account:
This will create a new user and its home folder.
`[root@archiso /]# useradd -m -g users -G wheel -s /bin/bash MYUSERNAME`

Set the password of user MYUSERNAME:
`[root@archiso /]# passwd MYUSERNAME`

##### Add the new user to sudoers:
If you want a root privilege in the future by using the sudo command, you should grant one yourself:
```bash
[root@archiso /]# EDITOR=nano visudo
```

Uncomment the line (Remove #):
```bash
%wheel ALL=(ALL) ALL
```

### Exit chroot and reboot:
Exit the chroot environment by typing exit or pressing Ctrl + d. You can also unmount all mounted partition after this.
`umount -R /mnt`

Finally, reboot.
'reboot'
