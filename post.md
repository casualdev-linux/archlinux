# swap
To check swap status, use:

$ swapon --show
Or to show physical memory as well as swap usage:

$ free -h

### Swap Partition
To set up a partition as Linux swap area, the mkswap(8) command is used. For example:

 mkswap /dev/sdxy
Warning: All data on the specified partition will be lost.
To enable the device for paging:

 swapon /dev/sdxy
To enable this swap partition on boot, add an entry to /etc/fstab:

UUID=device_UUID none swap defaults 0 0
where the device_UUID is the UUID of the swap space.

### Swap file
Use mkswap(8) to create a swap file the size of your choosing. For example, creating an 4 GiB swap file:

 mkswap -U clear --size 4G --file /swapfile
Activate the swap file:

 swapon /swapfile
Finally, edit the fstab configuration to add an entry for the swap file:

/etc/fstab
/swapfile none swap defaults 0 0

### Zram
zram-generator provides a systemd-zram-setup@.service unit to automatically initialize zram devices without users needing to enable/start the template or its instances. 

o create a zram swap device using zstd and half of the entire available ram, install zram-generator, then create /etc/systemd/zram-generator.conf with the following:

/etc/systemd/zram-generator.conf
[zram0]
zram-size = ram / 2
compression-algorithm = zstd


microcode
plymouth
linux-zen
grub

#### AUR helper

`sudo pacman -S git reflector`
`etc/xdg/reflector/reflector.conf`

```
sudo pacman -S --needed base-devel
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si
```
`kitty xf86-video-amdgpu mesa vulkan-radeon xdg-user-dirs xdg-utils`

`sddm wayland xorg-xwayland kwin plasma-wayland-session`

`pipewire pipewire-jack wireplumber qpwgraph`

 dosfstools: FAT32 support
 e2fsprogs: ext2/3/4 support [installed]
 exfat-utils: exFAT support
 exfatprogs: exFAT support (alternative to exfat-utils)
 f2fs-tools: F2FS support
 fatresize: FAT resize support
 jfsutils: JFS support
 nilfs-utils: nilfs support
 ntfs-3g: NTFS support [installed]
 reiserfsprogs: Reiser support
 udftools: UDF support
 xfsprogs: XFS support